const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const multer = require('multer');



const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/images/');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

const filefilter = function (req, file, cb) {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);//file accepted
  } else {
    cb(null, true); // file rejected
  }
}

const upload = multer({ storage: storage, fileFilter: filefilter });

const checkAuth = require('../middleware/check_auth')
const User = require('../models/user');

/* GET users listing. */

router.get('/', function (req, res, next) {
  User.find().select('username email userImage')
    .exec()
    .then(user => {
      res.status(200).json({
        users: user
      });
    }
    )
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
});

router.post('/signup', upload.single('userImage'), function (req, res) {
  User.find({ email: req.body.email }).exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(409).json({
          message: "Email exists"
        });
      } else {
        bcrypt.hash(req.body.password, 10, function (err, hash) {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            const user = new User({
              _id: mongoose.Types.ObjectId(),
              username: req.body.username,
              email: req.body.email,
              password: hash,
              country: req.body.country,
              city: req.body.city,
              userImage: req.file.path
            });
            user.save()
              .then(result => {
                console.log(result);
                return res.status(200).json({
                  message: "user created"
                });
              })
              .catch(err => {
                console.log(err);
                return res.status(500).json({
                  error: err
                });
              });
          }
        });
      }
    })
    .catch();

});

router.post('/login', function (req, res) {
  User.find({ email: req.body.email }).exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: "Auth failed"
        });
      } else {
        bcrypt.compare(req.body.password, user[0].password, function (err, result) {
          if (err) {
            return res.status(401).json({
              message: "Auth failed"
            });
          }
          if (result) {
            //console.log(process.env.JWT_KEY);
            const token = jwt.sign({
              email: user[0].email,
              userid: user[0]._id
            },
              "secret",
              {
                expiresIn: "1h"
              }
            );
            return res.status(200).json({
              message: "Auth successful",
              token: token

            });
          } else {
            res.status(401).json({
              message: "Auth failed"
            });
          }
        });
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });


});

router.delete('/:userid',checkAuth,function(req,res){
  User.remove({_id:req.params.userid})
  .exec()
  .then(result =>{
      console.log('User deleted');
      return res.status(200).json({
          message :"User deleted"
      });
  })
  .catch(err =>{
      console.log(err);
      return res.status(500).json({
          error: err
      });
  });
});


router.patch('/:userid',function(req,res){
  const updateOps = {};
  for(const ops of req.body){
    updateOps[ops.propName] = ops.value;
  }
  User.update({_id: req.params.userid}, {$set: updateOps})
  .exec()
  .then(result=>{
    res.status(200).json({
      message: "User Updated"
    })
  })
  .catch(err=>{
    res.status(500).json({
      error:err,
      message: "User doesn't update"
    });
  });
});

module.exports = router;
