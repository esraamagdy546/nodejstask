const mongoose = require('mongoose');

const schema = mongoose.Schema;
const userSchema = new schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {type:String, required:true},
    email :
    {
        type:String,
        required:true,
        unique:true,
        match:/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password :{type:String, required:true},
    country : {type:String, required:true},
    city : {type:String, required:true},
    userImage: {type: String, required: true}
});

const User = mongoose.model('User',userSchema);

module.exports = User;